# Multi-Threaded TCP Server

Multi-Threaded TCP Server is Operating System Project (January 2019)

This project was worth 25% of the 5 credit module. It had simple requirements: create a Client-Server communication app. But even if this project requirement was simple I take a time to redesign it.

### About project (extras):
- The project was redesigned, to busy-client instead of busy-server.
- Project design is based on REST constraints. Because of these two, calls to the server was reduced more than half.
- Completely separated UI and backend. UI can be easily replaced without any effort. 
- The project is written using Java 10 features. Why not learn something new if there is a chance to do so.
- Custom CLI UI.

In my opinion, the biggest this project extra is redesign.
```bash
# From:
server> Enter username:
client> Mindaugas
server> Enter password:
client> pa$$word
server> Pick one of those options:
        [1] Add bug
            ...
client> 1
    ...
# I think you got Idea  
# Changed to:
# UI collects all necessary data from the user. Then...
client> LOGIN, Mindaugas, pa$$word  # it was a simple project so it didn't require any security or input verification
server> OK
client> ADD-BUG, ... bug derails here

# I hope you able to see the big difference

```

I have been asked why I went such trouble for such a small mark. My answer:
- It was easier to implement, because "Good design - less time spend on fixing bugs".
- It based on REST constraints, which is always good to follow.
- It is better to do stuff right because bad habits stick faster.