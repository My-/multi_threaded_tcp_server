package sw.server;

import sw.models.Bug;
import sw.models.User;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class Server {
    private  int port;
    private static final int CONNECTIONS = 10;

    private static boolean allowNewConnetions = true;
    private static int cpuThreds = Runtime.getRuntime().availableProcessors();
    private static ExecutorService pool = Executors.newWorkStealingPool();

    public Server(int port){
        this.port = port;
    }


    /**
     * Starts server and liens on socket. For eac connection create new thread.
     * @throws IOException if problems wit connection
     */
    public void start() throws IOException {
        var serverSocket = new ServerSocket(this.port, Server.CONNECTIONS);

        Predicate<Optional<Socket>> keepRunning = it -> allowNewConnetions;

        Supplier<Optional<Socket>> socketSupplier = ()-> {
            Optional<Socket> s = Optional.empty();
            try { s = Optional.of(serverSocket.accept()); }
            catch (IOException e) { e.printStackTrace(); }
            return s;
        };

        Consumer<ConnectionHandler> serveClient = handler -> {
            pool.submit(handler);
        };

        System.out.println("Server listens on port: "+ this.port);
        Stream.generate(socketSupplier)
                .takeWhile(keepRunning)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(ConnectionHandler::of)
                .forEach(serveClient);

    }

    /**
     * Stops allowing new connections
     */
    public void stop() {
        Server.allowNewConnetions = false;
    }

    /**
     * Main
     * @param args
     */
    public static void main(String[] args) {
        try {
            new Server(2004).start();
        }
        catch (IOException e) {  e.printStackTrace(); }
    }




}
