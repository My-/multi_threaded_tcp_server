package sw.server;

import sw.models.Bug;
import sw.models.Pages;
import sw.models.User;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

public class App {
    private static final String USER_DB = "user.csv";
    private static final String BUG_DB = "bug.csv";

    private Set<User> users = new HashSet<>();
    private Set<Bug> bugs = new HashSet<>();
    private int bugCounter = 3;

    private ReentrantLock lock = new ReentrantLock(true);
    private Condition notReady = lock.newCondition();

    // creating singleton
    private static App instance = new App();
    private App(){
        loadDB();
        addInitialData();

        writeBugDB();
        writeUserDB();

        System.out.println(users);
        System.out.println(bugs);
    }
    public static App getInstance(){
        return App.instance;
    }

    // add initial data to play with
    private void addInitialData(){
        // add users
        users.add(User.of("Mindaugas", 1, "mindaugas@awesome.os", "All"));
        users.add(User.of("John Doe", 2, "john@doe.os", "All"));
        // add bugs
        bugs.add(Bug.of(1, "Windows", Bug.Platform.WINDOWS, "Updating blocks usability", Bug.Status.OPEN ));
        bugs.add(Bug.of(2, "VisualStudio", Bug.Platform.WINDOWS, "Are very very slow. crashes often.", Bug.Status.OPEN ));
        bugs.add(Bug.of(3, "Linux", Bug.Platform.UNIX, "\"Very fast and free and makes us miserable\" - Microsoft", Bug.Status.OPEN ));
    }

    private void loadDB(){
        try {
            var pathUserDB = Paths.get("res", App.USER_DB);
            users = Files.lines(pathUserDB)
                    .map(it -> it.split(Pages.DATA_SPLICER))
                    .map(User::of)
                    .collect(Collectors.toSet());

            var pathBugDB = Paths.get("res", App.BUG_DB);
            bugs = Files.lines(pathBugDB)
                    .map(it -> it.split(Pages.DATA_SPLICER))
                    .map(Bug::of)
                    .peek( bug ->{
                        if( bug.getId() > bugCounter ){
                            bugCounter = bug.getId();
                        }
                    })
                    .collect(Collectors.toSet());
        }
        catch (IOException e) {
            e.printStackTrace();
        }

    }



    //** User DB **//

    /**
     * Checks if DB has given user.
     * @param user the user we looking for
     * @return {@code true} if user found.
     */
    public synchronized boolean contains(User user){
//        lock.lock();

        var R = this.users.contains(user);

//        lock.unlock();
        System.out.println("containsUser(): "+ R);
        return R;
    }

    /**
     * Adds given user to DB.
     * @param user the user we want to add.
     * @return true if user was added.
     */
    public synchronized boolean add(User user){
//        lock.lock();
//        try {
//            notReady.await();
//        }
//        catch (InterruptedException e) { e.printStackTrace();  }

        var R = this.users.add(user);

//        notReady.signal();
//        lock.unlock();
        System.out.println("addUser(): "+ R);
        return R;
    }

    /**
     * Checks if email is unique.
     * @param user the email we checking.
     * @return {@code true} if email wasn't found.
     */
    public synchronized boolean uniqueEmail(User user) {
//        lock.lock();

        var R = users.stream()
                .noneMatch(it -> it.getEmail().equals(user.getEmail()));
//                .anyMatch(it -> it.getEmail().equals(user.getEmail()));

//        lock.unlock();
        System.out.println("uniqueEmail(): "+ R);
        return R;
    }

    /**
     * Gets user from DB by his id.
     * @param id the id of the user.
     * @return user
     */
    public synchronized Optional<User> getUser(int id){
//        lock.lock();

        var user = this.users.stream().findFirst();

//        lock.unlock();
        System.out.println("getUser(): "+ user);
        return user;
    }



    //** Bug DB **//

    public synchronized Set<Bug> getBugs(){
//        lock.lock();
        var bugs = new HashSet<>(this.bugs);
//        lock.unlock();
        System.out.println("getBugs(): "+ bugs);
        return bugs;
    }

    /**
     * Checks if bug exist in DB
     * @param bug the bug we looking for
     * @return {@code true} if bug already exist
     */
    public synchronized boolean contains(Bug bug){
//        lock.lock();

        var R = this.bugs.contains(bug);

//        lock.unlock();
        System.out.println("containsBug(): "+ R);
        return R;
    }

    /**
     * Adds bug to DB.
     * @param bug the bug we want to add.
     * @return {@code true} if bug was added.
     */
    public synchronized boolean add(Bug bug){
//        lock.lock();

//        try { notReady.await(); }
//        catch (InterruptedException e) { e.printStackTrace();}

        var R = this.bugs.add(bug);

//        notReady.signal();
//        lock.unlock();
        System.out.println("addBug(): "+ R);
        return R;
    }

    /**
     * Gets next bug id.
     * @return next bug id.
     */
    public synchronized int getId(){
//        lock.lock();

//        try { notReady.await(); }
//        catch (InterruptedException e) { e.printStackTrace();}

        var id = ++this.bugCounter;
//        lock.unlock();
        System.out.println("getId(): "+ id);
        return id;
    }

    /**
     * Gets bug fro DB by id.
     * @param bugId the bugs id.
     * @return bug
     */
    public synchronized Optional<Bug> getBug(int bugId){
//        lock.lock();
        var R = this.bugs.stream()
                .filter(bug -> bug.getId() == bugId)
                .findAny();
//        lock.unlock();
        System.out.println("getBug(): "+ R);
        return R;
    }

    public static void main(String[] args) {
        App.getInstance();
    }

    public synchronized void writeUserDB(){
        try {
            Files.write(Paths.get("res", App.USER_DB),
                    (Iterable<String>)users.stream().map(User::toData)::iterator );
        }
        catch (IOException e) { e.printStackTrace(); }

    }

    public synchronized void writeBugDB(){
        try {
            Files.write(Paths.get("res", App.BUG_DB),
                    (Iterable<String>)bugs.stream().map(Bug::toData)::iterator );
        }
        catch (IOException e) { e.printStackTrace(); }

    }

}
