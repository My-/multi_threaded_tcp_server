package sw.server;

import sw.models.Bug;
import sw.models.Messenger;
import sw.models.Pages;
import sw.models.User;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Arrays;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class ConnectionHandler implements Runnable {
    private Socket socket;
    private App app = App.getInstance(); // Singleton instance
    private Messenger msg;
    private ObjectOutputStream out;
    private ObjectInputStream in;


    private ConnectionHandler(Socket socket){
        System.out.print("New Client socket: "+ socket);
        this.socket = socket;

        try {
            out = new ObjectOutputStream(socket.getOutputStream());
            out.flush();
            in = new ObjectInputStream(socket.getInputStream());
            this.msg = new Messenger(in, out);
        } catch (IOException e) {
            System.err.println("Connection lost...");
//            e.printStackTrace();
            try {
                out.close();
                in.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
        System.out.println(" ... was created");
    }

    /**
     * Creates {@link Runnable} instance of {@code ConnectionHandler} on a given
     * socket.
     *
     * @param socket the connection socket.
     */
    public static ConnectionHandler of(Socket socket){
        return new ConnectionHandler(socket);
    }

    void clientHandler(){
        var req = new String[]{};
        do{
            req =  msg.read();

            switch (req[0]){
                case Pages.HELLO: sayHi(req); break;
                case Pages.LOGIN: login(req); break;
                case Pages.REGISTER: register(req); break;
                case Pages.ADD_BUG: addBug(req); break;
                case Pages.ASSIGN_BUG: assignBug(req); break;
                case Pages.SHOW_FREE_BUGS: getFreeBugs(req); break;
                case Pages.SHOW_ALL_BUGS: getAllBugs(req); break;
                case Pages.UPDATE_BUG_STATUS: updateBugStatus(req); break;
                case Pages.UPDATE_BUG_DESCRIPTION: updateBugDescription(req); break;
                case Pages.UPDATE_BUG_ASSIGNER: assignBug(req); break;
                default:
                    System.out.println("Unknown request: "+ Arrays.toString(req));
            }

        }while (!req[0].equals(Pages.EXIT));

        System.out.println("Client closed on socket: "+ socket);
    }

    private void sayHi(String[] req) {
        System.out.println("Client says: "+ Arrays.toString(req));
        msg.send(Pages.HELLO + ", Client");
        System.out.println("Server responded");
    }

    /**
     * Checks if user exist with given details.
     * @param req the request from user.
     */
    void login(String[] req){
        System.out.println("\nLogin : "+ Arrays.toString(req));
        var id = Integer.parseInt(req[1]);
        var email = req[2];
        var user = User.of(id, email);

        if( app.contains(user) && !app.uniqueEmail(user) ){
            msg.send(Pages.OK +", "+ app.getUser(id).get());
        }
        else {
            msg.send(Pages.NOT);
        }
        System.out.println(" ...done.");
    }

    /**
     * Registers user. Adds to DB.
     * @param req the request from user.
     */
    void register(String[] req) {
        System.out.println("\nRegister : "+ Arrays.toString(req));
        var name = req[1];
        var id = req[2];
        var email = req[2];
        var department = req[3];
        var user = User.of(name, Integer.parseInt(id), email, department);
        var err = Pages.NOT +", ";

        if( app.contains(user) ){ err +=" [Id] in use"; }
        System.out.println(!app.uniqueEmail(user));
        if( !app.uniqueEmail(user) ){ msg.send(err + " [email] in use"); }
        else{
            app.add(user);
            msg.send(Pages.OK);
            app.writeUserDB();
        }
        System.out.println(" ...done.");
    }

    /**
     * Add bug to DB.
     * @param req the request from user.
     */
    void addBug(String[] req) {
        System.out.println("\nAdd Bug: "+ Arrays.toString(req));
        var id = app.getId();
        var appName = req[1];
        var platform = Bug.Platform.of(req[2]);
        var problem = req[3];
        var bug = Bug.of(id, appName, platform, problem, Bug.Status.of("o"));

        if( app.add(bug) ){
            msg.send(Pages.OK);
            app.writeBugDB();
        }
        else{ msg.send(Pages.NOT); }
        System.out.println(" ...done.");
    }


    /**
     * Assigns user to the bug
     * @param req the request from user.
     */
    void assignBug(String[] req) {
        System.out.println("\nAssign Bug: "+ Arrays.toString(req));
        var bugId = Integer.parseInt(req[1]);
        var userId = Integer.parseInt(req[2]);

        var user = app.getUser(userId);
        var bug = app.getBug(bugId);

        if( user.isPresent() && bug.isPresent() ){
            bug.get().setAssigner(user.get());
            bug.get().setStatus(Bug.Status.ASSIGNED);
            msg.send(Pages.OK);
            app.writeBugDB();
        }
        else {
            var m = Pages.NOT;
            if( !user.isPresent() ){ m += " [User] not found"; }
            if( !bug.isPresent() ){ m += " [Bug} not found"; }
            msg.send(m);
        }
        System.out.println(" ...done.");
    }

    String getBugTable(Predicate<Bug> filter){
        return    "|----:|---------:|-------------:|---------:|:--------:|:-------------------, "
                + "|  id |   Time   |      App     | Platform |  Status  |     Description    , "
                + "|----:|---------:|-------------:|---------:|:--------:|:-------------------"
                + app.getBugs()
                        .stream()
                        .filter(filter)
                        .map(Bug::toString)
                        .collect(Collectors.joining(Pages.DATA_SPLICER, Pages.DATA_SPLICER, ""));
    }

    void getFreeBugs(String[] req) {
        System.out.println("\nGet Free Bugs: "+ Arrays.toString(req));
        Predicate<Bug> noAssigner = bug -> Objects.isNull(bug.getAssigner());
        msg.send(Pages.OK + Pages.DATA_SPLICER + getBugTable(noAssigner));
        System.out.println(" ...done.");
    }

    void getAllBugs(String[] req) {
        System.out.println("\nGet All Bugs: "+ Arrays.toString(req));
        msg.send(Pages.OK + Pages.DATA_SPLICER + getBugTable(it -> true));
        System.out.println(" ...done.");
    }

    void updateBugStatus(String[] req) {
        System.out.println("\nUpdate Bug Status: "+ Arrays.toString(req));
        var bugId = Integer.parseInt(req[1]);
        var newStatus = Bug.Status.of(req[2]);
        var bug = app.getBug(bugId);

        if( bug.isPresent() ){
            bug.get().setStatus(newStatus);
            msg.send(Pages.OK);
            app.writeBugDB();
        }
        else {
            msg.send(Pages.NOT + ", Bug with id "+ bugId +" not found.");
        }
        System.out.println(" ...done.");
    }

    void updateBugDescription(String[] req) {
        System.out.println("\nUpdate Bug Description: "+ Arrays.toString(req));
        var bugId = Integer.parseInt(req[1]);
        var newDescription = req[2];
        var bug = app.getBug(bugId);

        if( bug.isPresent() ){
            bug.get().setProblemDescription(bug.get().getProblemDescription() +" # "+ newDescription);
            msg.send(Pages.OK);
            app.writeBugDB();
        }
        else {
            msg.send(Pages.NOT + ", Bug with id "+ bugId +" not found.");
        }
        System.out.println(" ...done.");
    }



    @Override
    public void run() {
        clientHandler();
    }


}
