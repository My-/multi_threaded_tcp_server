package sw.client;

import sw.models.Messenger;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class Client {
    private static final String IP_ADDRESS = "127.0.0.1";
    private static final String PORT = "2004";

    private Socket requestSocket;
    private ObjectOutputStream out;
    private ObjectInputStream in;

    public static void main(String[] args) {
        try {
            new Client().connect(Client.IP_ADDRESS, Client.PORT);
        } catch (IOException e) {
//            e.printStackTrace();
            System.out.println("Connection lost.. buy!");
        }
    }



    public void connect(String ipAddress, String port) throws IOException {
        try {
            requestSocket = new Socket(ipAddress, 2004);

            System.out.println(String.format(
                    "Connecting to %s:%s ...", ipAddress, port
            ));

            out = new ObjectOutputStream(requestSocket.getOutputStream());
            out.flush();
            in = new ObjectInputStream(requestSocket.getInputStream());

            var msg = new Messenger(in, out);
            var ui = new UI(msg);
            System.out.println("...done. opening menu.");
            ui.mainPage();
        }
        catch( IOException e ){
            e.printStackTrace();
            out.close();
            in.close();
        }

    }


}
