package sw.client;

import sw.models.Pages;
import sw.models.Messenger;

import java.util.Arrays;
import java.util.Optional;
import java.util.Scanner;
import java.util.StringJoiner;
import java.util.stream.Stream;

public class UI {
    private static Scanner scanner = new Scanner(System.in);

    private Messenger msg;

    public UI(Messenger messenger){
        this.msg = messenger;
    }

    /**
     * Starting point of UI
     */
    public void mainPage(){
        var page = UI.header("Main") +
                "\n"+
                UI.option('1', "Test connection")
                + "   [2] - Login\n" +
                "   [3] - Register\n" +
                UI.footerExit();

        var res = "";

        do{
            System.out.println(page);

            switch( UI.getChar() ){
                case '1': testConnection(); break;
                case '2': login(); break;
                case '3': register(); break;
                case 'x': if( UI.confirmation("Do you want Quit program?") ){
                    msg.send(Pages.EXIT);
                    res = msg.read()[0];
                }
                default:
                    System.out.println("\tWrong choice!");
            }

        }while (!res.equals(Pages.EXIT));

        System.out.println("Bye!");
    }

    private void testConnection() {
        System.out.print("Sending 'Hi' to server...");
        msg.send(Pages.HELLO + ", server");
        var res = msg.read();
        System.out.println(" done!");
        System.out.println("Server says: "+ Arrays.toString(res));
    }

    /**
     * Login Page
     */
    private void login() {
        var empID = "";
        var empEmail= "";
        char choice;

        do{
            System.out.println(
                    UI.header("Login Page") +
                            UI.option('1', "Employee ID: "+empID) +
                            UI.option('2', "Email: "+ empEmail) +
                            UI.footer(true, false, UI.option('L', "Login"))
            );

            choice = UI.getChar();

            switch (choice){
                case '1': empID = UI.inputOptional("Enter Employee ID").orElse(empID); break;
                case '2': empEmail = UI.inputOptional("Enter Email").orElse(empEmail); break;
                case 'l':
                    msg.send(Pages.LOGIN+ ", "+ empID +", "+ empEmail);
                    var res = msg.read();

                    if( res[0].equals(Pages.OK) ){
                        choice = '0';
                        clientMenu(res[1]); // <--- go to client menu
                    }
                    else{
                        System.out.println("Wrong Id or email.");
                    }
                    break;
            }

        }while(choice != '0');
    }

    /**
     * Register Page
     */
    private void register() {
        var name = "";
        var empID = "";
        var empEmail= "";
        var department = "";
        char choice;

        do{
            System.out.println(
                    UI.header("Register Page") +
                            UI.option('1', "Employee name: "+name) +
                            UI.option('2', "Employee ID: "+empID) +
                            UI.option('3', "Email: "+ empEmail) +
                            UI.option('4', "Department: "+ department) +
                            UI.footer(true, false, UI.option('R', "Register"))
            );
            choice = UI.getChar();
            switch (choice){
                case '1': name = UI.inputOptional("Enter Employee name").orElse(name); break;
                case '2': empID = UI.inputOptional("Enter Employee ID").orElse(empID); break;
                case '3': empEmail = UI.inputOptional("Enter Email").orElse(empEmail); break;
                case '4': department = UI.inputOptional("Enter Department").orElse(department); break;
                case 'r':
                    if( name.isEmpty() || empEmail.isEmpty() || department.isEmpty() ||  empID.isEmpty() ){
                        System.out.println("No empty fields");
                        break;
                    }

                    msg.send(String.format(
                            "%s, %s, %s, %s, %s%n", Pages.REGISTER, name, empID, empEmail, department
                    ));

                    var res = msg.read();

                    if( res[0].equals(Pages.OK) ){
                        choice = '0';
                    }
                    else{ System.out.println(res[1]); }
            }
        }while(choice != '0');
    }

    /**
     * Client Page
     */
    private void clientMenu(String name) {
        var page = UI.header("HI: "+ name)+
                "\n" +
                UI.option('1', "Add bug record") +
                UI.option('2', "Assign bug") +
                UI.option('3', "View NOT assigned bugs") +
                UI.option('4', "Show all bug reports") +
                UI.option('5', "Update bug") +
                UI.footerBack();
        var choice = ' ';

        do{
            System.out.println(page);
            choice = UI.getChar();
            switch (choice){
                case '1': addBug(); break;
                case '2': assignBug(); break;
                case '3': viewFreeBugs(); break;
                case '4': viewAllBugs(); break;
                case '5': updateBug(); break;
            }
        }while(choice != '0');
    }



    /**
     * Add bug page.
     */
    void addBug() {
        var applicationName = "";
        var platform = "";
        var problemDescription = "";
        var choice = ' ';

        do{
            System.out.println(
                    UI.header("Add Bug") +
                    "\n" +
                    UI.option('1', "Application name: " + applicationName) +
                    UI.option('2', "Platform: "+ platform) +
                    UI.option('3', "Problem Description: "+ problemDescription) +
                    UI.footer(true, false, UI.option('+', "Save"))
            );
            choice = UI.getChar();
            switch (choice){
                case '1': applicationName = UI.inputOptional("Enter Application Name").orElse(applicationName); break;
                case '2': platform = UI.inputOptional("Enter Platform").orElse(platform); break;
                case '3':
                    scanner.nextLine();
                    System.out.println(""
                            + UI.header("Describe problem")
                            + UI.footerBack()
                    );
                    problemDescription = scanner.nextLine();
                    break;
                case '+':
                    if( applicationName.isEmpty() || platform.isEmpty() || problemDescription.isEmpty()  ){
                        System.out.println("No empty fields");
                        break;
                    }
                    var sj = new StringJoiner(Pages.DATA_SPLICER);
                    sj.add(Pages.ADD_BUG);
                    sj.add(applicationName);
                    sj.add(platform);
                    sj.add(problemDescription);
                    msg.send(sj.toString());
                    if( msg.read()[0].equals(Pages.OK) ){
                        choice = '0';
                    }
                    break;
            }
        }while(choice != '0');

    }


    private void assignBug() {
        var choice = ' ';
        var bugId = "";
        var userId = "";

        do{
            System.out.println(""+
                    UI.header("Assign Bug") +
                    "\n" +
                    UI.option('1', "Bug Id: "+ bugId) +
                    UI.option('2', "User Id: "+ userId) +
                    UI.footer(true, false, UI.option('+', "Save"))
            );

            switch (UI.getChar()){
                case '1': bugId = UI.inputOptional("Enter Bug Id").orElse(bugId); break;
                case '2': userId = UI.inputOptional("Enter User Id").orElse(userId); break;
                case '+':
                    if( bugId.isEmpty() || userId.isEmpty() ){
                        System.out.println("No empty fields");
                        break;
                    }
                    var sj = new StringJoiner(Pages.DATA_SPLICER);
                    sj.add(Pages.ASSIGN_BUG);
                    sj.add(bugId);
                    sj.add(userId);
                    msg.send(sj.toString());
                    var res = msg.read();
                    if( res[0].equals(Pages.NOT)){
                        System.out.println("Error: "+ res[1]);
                    }
                    else{
                        System.out.println("Success");
                        choice = '0';
                    }
            }
        }while( choice != '0');
    }


    private void viewFreeBugs() {
        msg.send(Pages.SHOW_FREE_BUGS);
        var res = msg.read();

        if( res[0].equals(Pages.NOT) ){
            System.out.println("Error: "+ res[1]);
        }
        else{
            System.out.println();
            Stream.of(res)
                    .skip(1L)
                    .forEach(System.out::println); // TODO: fix not printing
        }
    }

    private void viewAllBugs() {
        msg.send(Pages.SHOW_ALL_BUGS);
        var res = msg.read();

        if( res[0].equals(Pages.NOT) ){
            System.out.println("Error: "+ res[1]);
        }
        else{
            System.out.println();
            Stream.of(res)
                    .skip(1)
                    .forEach(System.out::println);
        }

    }

    private void updateBug() {
        var choice = ' ';
        var bugId = "";

        do{
            System.out.println("" +
                    UI.header("Update Bug: "+ bugId)
                    + UI.option('1', "Bug Id: "+ bugId)
                    + (bugId.length() > 0 ? UI.option('2', "Status") : "")
                    + (bugId.length() > 0 ? UI.option('3', "Append to the problem description") : "")
                    + (bugId.length() > 0 ? UI.option('4', "Change Assigned Engineer") : "")
                    + UI.footerBack()
            );
            choice = UI.getChar();

            switch (choice){
                case '1': bugId = UI.inputOptional("Enter bug id").orElse(bugId); break;
                case '2': if( bugId.length() > 0 ){ updateBugStatus(bugId); } break;
                case '3': if( bugId.length() > 0 ){ updateBugDescription(bugId); } break;
                case '4': if( bugId.length() > 0 ){ updateBugAssigner(bugId); } break;
            }

        }while (choice != '0');
    }

    private void updateBugStatus(String bugId) {
        var newStatus = UI.inputOptional("Enter new bug status (Open, Close, Assign):");
        if( newStatus.isPresent() ){
            var sj = new StringJoiner(Pages.DATA_SPLICER);
            sj.add(Pages.UPDATE_BUG_STATUS);
            sj.add(bugId);
            sj.add(newStatus.get());

            msg.send(sj.toString());
            var res = msg.read();

            if( res[0].equals(Pages.OK) ){ System.out.println("OK"); }
            else{ System.out.println("Error: "+ res[1]); }
        }
    }

    private void updateBugDescription(String bugId) {
        scanner.nextLine(); // flush the buffer
        System.out.println("Enter bug Description to append");
        var newDescription = scanner.nextLine();

        var sj = new StringJoiner(Pages.DATA_SPLICER);
        sj.add(Pages.UPDATE_BUG_DESCRIPTION);
        sj.add(bugId);
        sj.add(newDescription);

        msg.send(sj.toString());
        var res = msg.read();

        if( res[0].equals(Pages.OK) ){ System.out.println("OK"); }
        else{ System.out.println("Error: "+ res[1]); }

    }

    private void updateBugAssigner(String bugId) {
        var newStatus = UI.inputOptional("Enter new bug Assigner id: ");
        if( newStatus.isPresent() ){
            var sj = new StringJoiner(Pages.DATA_SPLICER);
            sj.add(Pages.UPDATE_BUG_ASSIGNER);
            sj.add(bugId);
            sj.add(newStatus.get());

            msg.send(sj.toString());
            var res = msg.read();

            if( res[0].equals(Pages.OK) ){ System.out.println("OK"); }
            else{ System.out.println("Error: "+ res[1]); }
        }
    }


    //********************** Helpers **********************//




    // Yes / No confirmation menu
    private static boolean confirmation(String message){
        String page = UI.header(message) +
                "   [Y] - Yes\n" +
                "   [N] - No\n"+
                UI.footer(false, false, "");

        do {
            System.out.println(page);
            switch (UI.getChar()) {
                case 'y': return true;
                case 'n': return false;
            }
        } while (true);
    }

    private static Optional<String> inputOptional(String headerTitle) {
        System.out.println(
                UI.header(headerTitle)+
                        footerBack()
        );

        var input = UI.getUserInput();

        return input.charAt(0) == '0' ? Optional.empty() : Optional.of(input);
    }

    private static String getUserInput(){
        String input = UI.scanner.next().trim().toLowerCase();

        return input;
    }

    private static char getChar(){
        return UI.scanner.next().trim().toLowerCase().charAt(0);
    }

    private static String header(String title){
        return "\n" +
                "========================\n" +
                "   "+ title +"\n" +
                "========================\n";
    }

    private static String option(char choice, String menu){
        return String.format("   [%c] - %s%n", choice, menu);
    }

    private static String footerBack(){
        return UI.footer(true, false, "");
    }

    private static String footerExit(){
        return UI.footer(false, true, "");
    }

    private static String footerCustom(String customOption){
        return UI.footer(false, false, customOption);
    }

    private static String footer(boolean goBackOption, boolean exitOption, String customOption){
        return "------------------------\n" +
                (goBackOption ? "   [0] - Back\n" : "") +
                (exitOption ? "   E[x]it\n" : "") +
                (customOption.isEmpty() ? "" : "" +customOption +"\n");

    }
}
