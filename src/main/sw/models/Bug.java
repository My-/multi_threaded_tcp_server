package sw.models;

import java.security.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;
import java.util.StringJoiner;


public class Bug {
    private int id;
    private String appName;
    private String timestamp;
    private Platform platform;
    private String problemDescription;
    private Status status;
    private User assigner;

    private Bug(int id, String appName, String timeStamp, Platform platform, String problemDescription, Status status) {
        this.id = id;
        this.appName = appName;
        this.timestamp = timeStamp;
        this.platform = platform;
        this.problemDescription = problemDescription;
        this.status = status;
    }

    public static Bug of(int id, String appName, Platform platform, String problemDescription, Status status){
        return new Bug(id, appName, getTimeStamp(Calendar.getInstance()), platform, problemDescription, status);
    }

    public static Bug of(String[] data){
        return new Bug(Integer.parseInt(data[0]), data[1], data[2], Platform.of(data[3]), data[4], Status.of(data[5]));
    }

    public String getAppName() {
        return appName;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public Platform getPlatform() {
        return platform;
    }

    public Status getStatus() {
        return status;
    }

    public int getId() {
        return id;
    }

    public String getProblemDescription() {
        return problemDescription;
    }

    public void setProblemDescription(String problemDescription) {
        this.problemDescription = problemDescription;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bug bug = (Bug) o;
        return id == bug.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {

        //                    |  id |   Time   |    App   | Platform | Status |     Description
        //                    |----:|---------:|---------:|---------:|:--------:|:-------------------
        return String.format("| %3s | %-8s | %-12s | %-8s | %8s | %20s",
                            id, timestamp, appName, platform, status, problemDescription );
    }

    private static String getTimeStamp(Calendar timestamp){
        return String.format("%s-%s-%s", timestamp.get(Calendar.DATE), timestamp.get(Calendar.MONTH), timestamp.get(Calendar.YEAR) );
    }

    public User getAssigner() {
        return assigner;
    }

    public void setAssigner(User assigner) {
        this.assigner = assigner;
    }

    public static String toData(Bug bug) {
        var sj = new StringJoiner(Pages.DATA_SPLICER);
        sj.add(bug.getId()+"");
        sj.add(bug.getAppName());
        sj.add(bug.getTimestamp());
        sj.add(bug.getPlatform().toString());
        sj.add(bug.getProblemDescription());
        sj.add(bug.getStatus().toString());
        return sj.toString();
    }

    public enum Platform {
        WINDOWS, UNIX, MAC;


        public static Platform of(String platform){
            switch (platform.toLowerCase()){
                case "w":
                case "win":
                case "windows": return Platform.WINDOWS;
                case "u":
                case "unix": return Platform.UNIX;
                case "m":
                case "mac": return Platform.MAC;
            }

            return Platform.WINDOWS;
        }
    }

    public enum Status {
        OPEN, ASSIGNED, CLOSE;

        public static Status of(String platform){
            switch (platform.toLowerCase()){
                case "o":
                case "open": return Status.OPEN;
                case "a":
                case "assigned": return Status.ASSIGNED;
                case "c":
                case "close": return Status.CLOSE;
            }

            return Status.OPEN;
        }
    }
}
