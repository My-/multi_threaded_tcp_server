package sw.models;

public class Pages {
    public static final String MAIN = "main";
    public static final String LOGIN = "login";
    public static final String REGISTER = "register";
    public static final String EXIT = "client-done-with-connection";
    public static final String CLIENT = "client";
    public static final String ADD_BUG = "add-bug";
    public static final String ASSIGN_BUG = "assign-bug";
    public static final String SHOW_FREE_BUGS = "show-free-bugs";
    public static final String SHOW_ALL_BUGS = "show-all-bugs";
    public static final String UPDATE_BUG = "update-bugs";

    public static final String OK = "all-good";
    public static final String NOT = "not-ok";
    public static final String DATA_SPLICER = ", ";
    public static final String UPDATE_BUG_STATUS = "update-bug-status";
    public static final String UPDATE_BUG_DESCRIPTION = "update-bug-description";
    public static final String UPDATE_BUG_ASSIGNER = "update-bug-assigner";
    public static final String HELLO = "hello";
}
