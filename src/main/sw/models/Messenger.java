package sw.models;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Arrays;

/**
 * Class responsible for sending messages to
 */
public class Messenger {
    private ObjectOutputStream out;
    private ObjectInputStream in;

//    public Messenger(){}

    public Messenger(ObjectInputStream in, ObjectOutputStream out){
        try {
            this.in = in;
            this.out = out;
            this.out.flush();
        }
        catch (IOException e) {
//            e.printStackTrace();
            System.err.println("Connection lost..");
        }
    }


    /**
     * Sends message.
     * @param msg the message to be send.
     */
    public void send(String msg) {
        try{
            out.writeObject(msg);
            out.flush();
            System.out.println("send(): "+ msg);
        } catch (IOException e) {
//            e.printStackTrace();
            System.err.println("Messenger.send() connection lost..");
        }
    }

    /**
     * Reads message.
     * @return the message spliced on {@link Pages}.{@code DATA_SPLICER}
     */
    public String[] read(){
        String serverResponse = "";

        try {
            serverResponse = (String) in.readObject();
        } catch (IOException | ClassNotFoundException e) {
//            e.printStackTrace();
            System.err.println("Messenger.read() connection lost...");
            serverResponse = Pages.EXIT;
            try {
                in.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

        var res = serverResponse.toLowerCase().trim().split(Pages.DATA_SPLICER);
        System.out.println("read(): "+ Arrays.toString(res));
        return res;
    }


}
