package sw.models;

import java.util.Objects;
import java.util.StringJoiner;

/**
 * Immutable class.
 */
public class User {
    private String name;
    private int id;
    private String email;
    private String department;

    private User(String name, int id, String email, String department) {
        this.name = name;
        this.id = id;
        this.email = email;
        this.department = department;
    }

    public static User of(String name, int id, String email, String department){
        return new User(name, id, email, department);
    }
    public static User of(int id, String email){
        return new User("John Doe", id, email, "");
    }
    public static User of(String[] data){
        return new User(data[0], Integer.parseInt(data[1]), data[2], data[3]);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public static String toData(User user){
        var sj = new StringJoiner(Pages.DATA_SPLICER);
        sj.add(user.getName());
        sj.add(user.getId()+"");
        sj.add(user.getEmail());
        sj.add(user.getDepartment());
        return sj.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public String toString() {
        return String.format("%s(%s) - %s - %s", name, id, email, department);
    }
}
